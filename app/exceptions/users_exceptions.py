class UserAlreadyExistsException(Exception):
    pass


class AttributesIsNotInstanceOfString(Exception):
    
    def __init__(self, dict):
        self.dict = dict

    def get_dict(self):
        return self.dict
        