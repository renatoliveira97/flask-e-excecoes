from flask import Flask, jsonify, request
from app.models.user_model import User
from app.exceptions.users_exceptions import UserAlreadyExistsException, AttributesIsNotInstanceOfString


def home_view(app: Flask):
    
    @app.get('/user')
    def get_all():

        data = User.get_all()

        return jsonify(data), 200

    @app.post('/user')
    def create_user():

        try:         
            data = request.get_json()

            User.check_if_the_database_exists_or_is_empty()
            User.check_if_the_attributes_is_instance_of_string(**data)

            user = User(**data)

            User.save(user)

            return jsonify({"data": user.__dict__}), 201

        except UserAlreadyExistsException:
            message = {"error": "User already exists"}
            return jsonify(message), 409

        except AttributesIsNotInstanceOfString as e:
            return jsonify(e.get_dict()), 400

        except TypeError as e:
            return f'message: {e.args}', 400
