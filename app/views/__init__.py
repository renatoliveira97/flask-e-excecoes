from flask import Flask
from app.views.home_view import home_view


def init_app(app: Flask):

    home_view(app)

    return app
    