from json import load, dump
from os.path import exists, getsize
from app.exceptions.users_exceptions import UserAlreadyExistsException, AttributesIsNotInstanceOfString

class User():

    def __init__(self, nome: str, email: str):
        self.nome = nome.title()
        self.email = email.lower()
        self.id = User.get_id()


    @staticmethod
    def get_all():

        User.check_if_the_database_exists_or_is_empty()

        with open('app/database/database.json', 'r') as json_file:
            data = load(json_file)        

        data = {"data": data} 

        return data 


    @staticmethod
    def save(user):

        with open('app/database/database.json', 'r') as json_file:
            data = load(json_file)

            for item in data:
                if user.__dict__["email"] == item["email"]:
                    raise UserAlreadyExistsException()

        data.append(user.__dict__)

        with open('app/database/database.json', 'w') as json_file:
            dump(data, json_file, indent = 4)


    @staticmethod
    def get_id():

        with open('app/database/database.json', 'r') as json_file:
            data = load(json_file)

        if data == [] or getsize('app/database/database.json') == 0:
            id = 1
        else:
            id = data[-1]['id'] + 1        

        return id

    
    @staticmethod
    def check_if_the_database_exists_or_is_empty():

        database_exists = exists('app/database/database.json')

        empty_list = []

        if not database_exists:
            with open('app/database/database.json', 'w') as json_file:
                dump(empty_list, json_file)

        if getsize('app/database/database.json') == 0:
            with open('app/database/database.json', 'w') as json_file:
                dump(empty_list, json_file)


    @staticmethod
    def check_if_the_attributes_is_instance_of_string(nome, email):

        if not isinstance(nome, str) or not isinstance(email, str):

            data = []

            if not isinstance(nome, str):
                data.append({"nome": str(type(nome)).replace("<class '", '').replace("'>", '')})

            if not isinstance(email, str):
                data.append({"email": str(type(email)).replace("<class '", '').replace("'>", '')})

            raise AttributesIsNotInstanceOfString({"wrong fields": data})
