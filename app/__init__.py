from flask import Flask
from app import views
from environs import Env


def create_app():

    env = Env()
    env.read_env()

    app = Flask(__name__)

    views.init_app(app)

    return app
    